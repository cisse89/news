import { MatcherFunction } from "@testing-library/react";

const multilineTextPartMatcher = (str: string): MatcherFunction => {
  return (content) => {
    return content.includes(str);
  };
};

export { multilineTextPartMatcher };
