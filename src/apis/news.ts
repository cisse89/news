import axios from "axios";

const apiKey = "eb20db8d11454342842349fdc26bfa71";

const newsApi = axios.create({
  baseURL: "https://newsapi.org/v2/top-headlines",
  params: {
    apiKey: apiKey,
  },
});

const getCountryNews = (country: string) => {
  return newsApi.get("", { params: { country } });
};

export default getCountryNews;
