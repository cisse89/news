import { Article } from "../types/Article.types";
import { useState } from "react";
import ArticleDetailed from "./ArticleDetailed";

interface MyProps {
  article: Article;
}
const ArticleListItem: React.FC<MyProps> = ({ article }) => {
  const [isArticleOpen, setArticleOpen] = useState<boolean>(false);
  return (
    <>
      <div className="item" onClick={() => setArticleOpen(true)}>
        <div className="header">{article.title}</div>
        {article.source.name}@ {article.publishedAt}
      </div>
      <ArticleDetailed
        article={article}
        isVisible={isArticleOpen}
        onCloseCallback={() => setArticleOpen(false)}
      />
    </>
  );
};

export default ArticleListItem;
