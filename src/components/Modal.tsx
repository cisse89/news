import React, { PropsWithChildren } from "react";
import ReactDOM from "react-dom";

interface MyProps {
  isShown: boolean;
  header: string;
  onDismiss: () => void;
}

const Modal: React.FC<PropsWithChildren<MyProps>> = ({ children, onDismiss, isShown, header }) => {
  const modalSelector = document.querySelector("#modal");

  const render = () => {
    return (
      <div
        className="ui dimmer modals visible active"
        onClick={onDismiss}
        style={{ position: "fixed" }}
      >
        <div
          className="ui standard modal visible active"
          onClick={(event) => event.stopPropagation()}
          style={{ minWidth: 250 }}
        >
          <div className="header">
            <div className="ui two column grid">
              <div className="row">
                <div className="fourteen wide column">{header}</div>
                <div className="two wide column">
                  <button className="ui right floated icon mini red button" onClick={onDismiss}>
                    <i className="close icon"></i>
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="content">{children}</div>
        </div>
      </div>
    );
  };

  if (modalSelector && isShown) {
    return ReactDOM.createPortal(render(), modalSelector);
  } else {
    return <></>;
  }
};

export default Modal;
