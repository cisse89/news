import { useContext } from "react";
import TranslationContext from "../languages/TranslationContext";
import { Article } from "../types/Article.types";
import Modal from "./Modal";

interface MyProps {
  article: Article;
  isVisible: boolean;
  onCloseCallback: () => void;
}

const ArticleDetailed: React.FC<MyProps> = ({ article, isVisible, onCloseCallback }) => {
  const translation = useContext(TranslationContext);

  return (
    <>
      <Modal header={article.title} isShown={isVisible} onDismiss={onCloseCallback}>
        <>
          <p className="date">By {article.author}</p>
          <p>{article.content}</p>
          <a href={article.url} target="_blank" rel="norederrer">
            {translation.articleDetailed.sourceUrlLabel}
          </a>
        </>
      </Modal>
    </>
  );
};

export default ArticleDetailed;
