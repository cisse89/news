import { setContentType } from "../redux/contentTypeSlice";
import { useAppDispatch, useAppSelector } from "../redux/hooks";
import Modal from "./Modal";
import React, { useContext, useState } from "react";
import TranslationContext from "../languages/TranslationContext";
import { getAvailableTranslations } from "../languages/translationMap";

interface MyProps {
  changeLanguageCallback: (s: string) => void;
}
const Header: React.FC<MyProps> = ({ changeLanguageCallback }) => {
  const dispatch = useAppDispatch();
  const contentType = useAppSelector((state) => state.contentType);
  const translation = useContext(TranslationContext);

  const [isAboutPopupVisible, setIsAboutPopupVisible] = useState<boolean>(false);

  const onChangeContentTypeButtonClick = () => {
    if (contentType === "list") {
      dispatch(setContentType("tile"));
    } else {
      dispatch(setContentType("list"));
    }
  };

  const onAboutButtonClick = () => {
    setIsAboutPopupVisible(true);
  };

  const renderChallengesPupup = () => {
    return (
      <Modal
        header={translation.aboutPopup.header}
        isShown={isAboutPopupVisible}
        onDismiss={() => setIsAboutPopupVisible(false)}
      >
        {translation.aboutPopup.content.map((line) => (
          <p key={line}>{line}</p>
        ))}
      </Modal>
    );
  };

  const renderTranslationIcons = () => {
    return Array.from(getAvailableTranslations()).map((lang) => {
      return (
        <i key={lang} className={`${lang} flag`} onClick={() => changeLanguageCallback(lang)}></i>
      );
    });
  };
  return (
    <div className="ui segment centered grid" style={{ backgroundColor: "lightblue" }}>
      <h2 className="row" style={{ marginBottom: 0 }}>
        <a href="/">NEWS</a>
      </h2>
      <div className="two column relaxed row">
        <button className="ui blue button" onClick={onChangeContentTypeButtonClick}>
          {translation.header.layoutButonLabel}
        </button>
        <button className="ui button teal" onClick={onAboutButtonClick}>
          {translation.header.aboutButtonLabel}
        </button>
      </div>
      <div className="row">{renderTranslationIcons()}</div>
      {renderChallengesPupup()}
    </div>
  );
};

export default Header;
