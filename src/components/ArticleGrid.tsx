import React from "react";
import { Article } from "../types/Article.types";
import ArticleTile from "./ArticleTile";

interface MyProps {
  articles: Array<Article>;
}

const ArticleGrid: React.FC<MyProps> = ({ articles }) => {
  return (
    <div className="ui doubling four column grid">
      {articles.map((el) => {
        return <ArticleTile key={el.title} article={el} />;
      })}
    </div>
  );
};

export default ArticleGrid;
