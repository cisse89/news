import { render, RenderResult } from "@testing-library/react";
import { Article } from "../types/Article.types";
import ArticleTile from "./ArticleTile";
import { multilineTextPartMatcher } from "../../test/customMatchers";

const testArticle: Article = {
  author: "test_author",
  publishedAt: "test_published",
  title: "test_title",
  url: "test_url",
  urlToImage: "",
  description: "test_description",
  content: "",
  source: {
    id: "",
    name: "test_source_name",
  },
};

describe("tests for ArticleTile component", () => {
  let articleTile: RenderResult;

  beforeEach(() => {
    articleTile = render(<ArticleTile article={testArticle} />);
  });

  it("should render article title", () => {
    articleTile.getByText(testArticle.title);
  });

  it("should render article source name", async () => {
    articleTile.getByText(multilineTextPartMatcher(testArticle.source.name));
  });

  it("should render article published date", () => {
    articleTile.getByText(multilineTextPartMatcher(testArticle.publishedAt));
  });

  it("should render article description", () => {
    articleTile.getByText(testArticle.description);
  });
});
