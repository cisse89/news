import { fireEvent, render, RenderResult, queryHelpers } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import Modal from "./Modal";

const queryByClassName = queryHelpers.queryByAttribute.bind(null, "class");

describe("tests for Modal component", () => {
  const modalRoot = document.createElement("div", {});
  modalRoot.setAttribute("id", "modal");
  document.body.appendChild(modalRoot);
  const testModalHeaderValue = "test_modal_header";

  it("should render modal with header when isShown set to true", () => {
    const closeCallback = jest.fn();

    const modal = render(
      <Modal header={testModalHeaderValue} isShown={true} onDismiss={closeCallback} />
    );
    modal.getByText(testModalHeaderValue);
  });

  it("should not render modal with header when isShown set to false", () => {
    const closeCallback = jest.fn();

    const modal = render(
      <Modal header={testModalHeaderValue} isShown={false} onDismiss={closeCallback} />
    );
    const modalHeader = modal.queryByText(testModalHeaderValue);
    expect(modalHeader).not.toBeInTheDocument();
  });

  it("should call onDismiss callback when close button clicked", () => {
    const closeCallback = jest.fn();

    const modal: any = render(
      <Modal header={testModalHeaderValue} isShown={true} onDismiss={closeCallback} />
    );

    const closeButton = modal.getByText(testModalHeaderValue).nextSibling.children[0];

    act(() => {
      fireEvent.click(closeButton);
    });

    expect(closeCallback).toBeCalledTimes(1);
  });
});
