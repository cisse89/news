import Header from "./Header";
import { renderWithRedux } from "../../test/test-utils";

import { fireEvent, act, screen } from "@testing-library/react";

describe("tests for Header component", () => {
  const changeLanguageCbMock = jest.fn();

  it("should render app name", () => {
    const header = renderWithRedux(<Header changeLanguageCallback={changeLanguageCbMock} />);
    header.getByText(/news/i);
  });

  it("should render change layout button", () => {
    const header = renderWithRedux(<Header changeLanguageCallback={changeLanguageCbMock} />);
    header.getByText(/Change layout/i);
  });

  it("should change content type if button was clicked", () => {
    const header = renderWithRedux(<Header changeLanguageCallback={changeLanguageCbMock} />);
    expect(header.store.getState().contentType).toEqual("list");
    const contentTypeButton = header.getByText(/change layout/i);

    act(() => {
      fireEvent.click(contentTypeButton);
    });

    expect(header.store.getState().contentType).toEqual("tile");
  });

  it("should show about popup when about button clicked", () => {
    const modalRoot = document.createElement("div", {});
    modalRoot.setAttribute("id", "modal");
    document.body.appendChild(modalRoot);

    const header = renderWithRedux(<Header changeLanguageCallback={changeLanguageCbMock} />);

    act(() => {
      fireEvent.click(header.getByText(/about/i));
    });

    screen.getByText(/react libraries/i);
  });
});
