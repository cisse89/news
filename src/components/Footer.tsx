import { useState, useEffect, useContext } from "react";
import TranslationContext from "../languages/TranslationContext";
import { useAppSelector } from "../redux/hooks";

const Footer = () => {
  const getTimeString = () => {
    const date = new Date();
    return `${date.getHours()}:${date.getMinutes()}`;
  };

  const numOfArticles = useAppSelector((state) => state.numOfArticles);
  const translation = useContext(TranslationContext);

  useEffect(() => {
    const timeCallback = () => {
      setHour(getTimeString());
    };

    const intervalId = setInterval(timeCallback, 30000);

    return () => {
      clearInterval(intervalId);
    };
  }, []);

  const [hour, setHour] = useState<string>(getTimeString);

  return (
    <>
      {hour} {translation.footer.totalArticlesLabel}
      {numOfArticles}
    </>
  );
};

export default Footer;
