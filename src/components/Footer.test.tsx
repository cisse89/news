import Footer from "./Footer";
import { renderWithRedux } from "../../test/test-utils";
import { act } from "react-dom/test-utils";
import { setNumOfArticles } from "../redux/numOfArticlesSlice";

describe("tests for Footer component", () => {
  it("should render total articles count", () => {
    const footer = renderWithRedux(<Footer />);
    footer.getByText(/total articles/i);
  });

  it("should update total articles count, when redux value changed", () => {
    const footer = renderWithRedux(<Footer />);
    footer.getByText(/total articles: 0/i);

    act(() => {
      footer.store.dispatch(setNumOfArticles(42));
    });

    footer.getByText(/total articles: 42/i);
  });

  it("should display current hour and minutes", () => {
    jest.useFakeTimers();
    jest.spyOn(global, "setInterval");

    const date = new Date();
    const dateString = `${date.getHours()}:${date.getMinutes()}`;
    const dateStringAfterMinute = `${date.getHours()}:${date.getMinutes() + 1}`;

    const footer = renderWithRedux(<Footer />);

    expect(setInterval).toHaveBeenCalledTimes(1);

    footer.getByText((content) => {
      return content.includes(dateString);
    });

    act(() => {
      jest.advanceTimersByTime(60000);
    });

    footer.getByText((content) => {
      return content.includes(dateStringAfterMinute);
    });

    jest.useRealTimers();
  });
});
