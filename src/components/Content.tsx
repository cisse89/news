import { useContext, useEffect, useState } from "react";
import { useLoaderData } from "react-router-dom";
import getCountryNews from "../apis/news";
import { useAppDispatch, useAppSelector } from "../redux/hooks";
import { setNumOfArticles } from "../redux/numOfArticlesSlice";
import { Article } from "../types/Article.types";
import ArticleGrid from "./ArticleGrid";
import ArticleList from "./ArticleList";

export async function loader(request: any) {
  const data = await getCountryNews(request.params.id);
  if (data.data.articles.length === 0) {
    throw new Response(`No articles for country: ${request.params.id}`, {
      status: 400,
      statusText: "Bad Request",
    });
  }

  return data.data.articles;
}

const Content = () => {
  const articles: Array<Article> = useLoaderData() as Array<Article>;
  const contentType = useAppSelector((state) => state.contentType);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(setNumOfArticles(articles.length));
  }, []);

  const renderArticles = () => {
    if (articles.length === 0) throw new Error();
    if (contentType === "list") {
      return <ArticleList articles={articles} />;
    } else {
      return <ArticleGrid articles={articles} />;
    }
  };
  return (
    <>
      <div className="twelve wide column">{renderArticles()}</div>
      <div className="ui divider hidden"></div>
    </>
  );
};

export default Content;
