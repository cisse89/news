import { render, RenderResult } from "@testing-library/react";
import { Article } from "../types/Article.types";
import { multilineTextPartMatcher } from "../../test/customMatchers";
import ArticleListItem from "./ArticleListItem";

const testArticle: Article = {
  author: "test_author",
  publishedAt: "test_published",
  title: "test_title",
  url: "test_url",
  urlToImage: "",
  description: "test_description",
  content: "",
  source: {
    id: "",
    name: "test_source_name",
  },
};

describe("tests for ArticleListItem component", () => {
  let articleListItem: RenderResult;

  beforeEach(() => {
    articleListItem = render(<ArticleListItem article={testArticle} />);
  });

  it("should render article title", () => {
    articleListItem.getByText(testArticle.title);
  });

  it("should render article source name", async () => {
    articleListItem.getByText(multilineTextPartMatcher(testArticle.source.name));
  });

  it("should render article published date", () => {
    articleListItem.getByText(multilineTextPartMatcher(testArticle.publishedAt));
  });
});
