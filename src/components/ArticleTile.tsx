import { Article } from "../types/Article.types";
import { useState } from "react";
import ArticleDetailed from "./ArticleDetailed";

interface MyProps {
  article: Article;
}
const ArticleTile: React.FC<MyProps> = ({ article }) => {
  const [isArticleOpen, setArticleOpen] = useState<boolean>(false);
  return (
    <div className="column">
      <div className="ui card" onClick={() => setArticleOpen(true)}>
        <div className="image">
          <img alt="" src={article.urlToImage} />
        </div>
        <div className="content">
          <div className="header" style={{ wordBreak: "break-word" }}>
            {article.title}
          </div>
          <div className="meta">
            <span className="date">
              {article.source.name}
              <br /> {article.publishedAt}
            </span>
          </div>
          <div className="description" style={{ wordBreak: "break-word" }}>
            {article.description}
          </div>
        </div>
      </div>

      <ArticleDetailed
        article={article}
        isVisible={isArticleOpen}
        onCloseCallback={() => setArticleOpen(false)}
      />
    </div>
  );
};

export default ArticleTile;
