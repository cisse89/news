import { Article } from "../types/Article.types";
import ArticleListItem from "./ArticleListItem";

interface MyProps {
  articles: Array<Article>;
}

const ArticleList: React.FC<MyProps> = ({ articles }) => {
  return (
    <div className="ui celled list">
      {articles.map((el) => {
        return <ArticleListItem key={el.title} article={el}></ArticleListItem>;
      })}
    </div>
  );
};

export default ArticleList;
