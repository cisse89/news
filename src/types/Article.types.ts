export type Article = {
  author: string;
  publishedAt: string;
  title: string;
  url: string;
  urlToImage: string;
  description: string;
  content: string;
  source: { id: string; name: string };
};
