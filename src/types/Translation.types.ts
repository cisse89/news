export default interface Translation {
  header: {
    layoutButonLabel: string;
    aboutButtonLabel: string;
  };
  footer: {
    totalArticlesLabel: string;
  };
  menu: {
    poland: string;
    unitedStates: string;
    france: string;
    italy: string;
    germany: string;
    unitedKingdom: string;
    japan: string;
    czechRepublic: string;
    portugal: string;
  };
  content: {
    errorLabel: string;
  };
  articleDetailed: {
    sourceUrlLabel: string;
  };
  aboutPopup: {
    header: string;
    content: Array<string>;
  };
  errorHandler: {
    message: string;
  };
  default: {
    message: string;
  };
}
