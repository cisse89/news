import Translation from "../types/Translation.types";

export const polish: Translation = {
  header: {
    layoutButonLabel: "Zmień styl",
    aboutButtonLabel: "O projekcie...",
  },
  footer: {
    totalArticlesLabel: "Liczba artykułów: ",
  },
  menu: {
    poland: "Polska",
    unitedStates: "Stany Zjednoczone",
    france: "Francja",
    italy: "Włochy",
    germany: "Niemcy",
    unitedKingdom: "Wielka Brytania",
    japan: "Japonie",
    czechRepublic: "Czechy",
    portugal: "Portugalia",
  },
  aboutPopup: {
    header: "O projekcie...",
    content: [
      "Projekt powstał w celu nauki i pokazania połaczenia wszystkich najpopularniejszych bibliotek z React :)",
      "Uzyte funkcje z reacta: functional components, context, hooki, portal",
      "Pozostałe biblioteki: React Router DOM, Redux Toolkit, axios, jest do UT",
      "Do elementów CSS użyto głównie gotowych rozwiązań z Semantic UI",
    ],
  },
  content: {
    errorLabel: "Proszę wybrać państwo z menu po lewej.",
  },
  articleDetailed: {
    sourceUrlLabel: "Wiecej TUTAJ",
  },
  errorHandler: {
    message: "Coś poszło nie tak :(",
  },
  default: {
    message: "Proszę wybrać państwo żeby wyświetlić artykuły.",
  },
};
