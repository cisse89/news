import Translation from "../types/Translation.types";
import { english } from "./english";
import { polish } from "./polish";

const transalationMap = new Map<string, Translation>([
  ["gb", english],
  ["pl", polish],
]);

export function getAvailableTranslations() {
  return transalationMap.keys();
}
export function getTranslation(code: string) {
  const translation = transalationMap.get(code);
  if (translation) {
    return translation;
  }

  return english;
}
