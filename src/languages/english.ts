import Translation from "../types/Translation.types";

export const english: Translation = {
  header: {
    layoutButonLabel: "Change layout",
    aboutButtonLabel: "About...",
  },
  footer: {
    totalArticlesLabel: "Total articles: ",
  },
  menu: {
    poland: "Poland",
    unitedStates: "United States",
    france: "France",
    italy: "Italy",
    germany: "Germany",
    unitedKingdom: "United Kingdom",
    japan: "Japan",
    czechRepublic: "Czech Republic",
    portugal: "Portugal",
  },
  aboutPopup: {
    header: "About...",
    content: [
      "Project created mainly for learning and to connect most of the popular react libraries :)",
      "Used react features: functional components, context, hooks, portal",
      "Other libraries: Raect Router Dom, Redux Toolkit, axios, jest for UT",
      "For layout and CSS used mostly solutions ready in Semantic UI",
    ],
  },
  content: {
    errorLabel: "Please select a countryb from the menu on the left side.",
  },
  articleDetailed: {
    sourceUrlLabel: "Full story HERE",
  },
  errorHandler: {
    message: "Something went wrong :(",
  },
  default: {
    message: "Please select a country to view articles.",
  },
};
