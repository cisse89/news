import React from "react";
import Translation from "../types/Translation.types";
import { english } from "./english";

export default React.createContext<Translation>(english);
