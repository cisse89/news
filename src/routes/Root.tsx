import { useContext } from "react";
import { Outlet, NavLink } from "react-router-dom";
import TranslationContext from "../languages/TranslationContext";
import Translation from "../types/Translation.types";
import "./Root.css";

const COUNTRIES = (translation: Translation) => [
  { code: "pl", name: translation.menu.poland },
  { code: "us", name: translation.menu.unitedStates },
  { code: "fr", name: translation.menu.france },
  { code: "it", name: translation.menu.italy },
  { code: "de", name: translation.menu.germany },
  { code: "gb", name: translation.menu.unitedKingdom },
  { code: "jp", name: translation.menu.japan },
  { code: "cz", name: translation.menu.czechRepublic },
  { code: "pt", name: translation.menu.portugal },
];

const Root = () => {
  const translation = useContext(TranslationContext);
  const prepareNavLinks = () => {
    return COUNTRIES(translation).map((country) => {
      return (
        <div key={country.code}>
          <NavLink
            className={({ isActive }) => (isActive ? "active" : "")}
            to={`country/${country.code}`}
          >
            <h4>
              <i className={`${country.code} flag`} />
              {country.name}
            </h4>
          </NavLink>
        </div>
      );
    });
  };
  return (
    <div className="ui two column grid container">
      <div className="four wide column">
        <nav>
          <ul>{prepareNavLinks()}</ul>
        </nav>
      </div>
      <div className="twelve wide column">
        <Outlet />
      </div>
    </div>
  );
};

export default Root;
