import { useContext } from "react";
import TranslationContext from "../languages/TranslationContext";

const DefaultContent = () => {
  const translation = useContext(TranslationContext);
  return <div>{translation.default.message}</div>;
};

export default DefaultContent;
