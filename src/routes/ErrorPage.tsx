import { useContext } from "react";
import { useRouteError } from "react-router-dom";
import TranslationContext from "../languages/TranslationContext";

const ErrorPage = () => {
  const error = useRouteError() as any;
  const translation = useContext(TranslationContext);

  return (
    <div className="ui raised segment">
      <h1>{translation.errorHandler.message}</h1>
      <p>
        {error.status} {error.statusText || error.message}
      </p>
      <p>
        <i>{error.data}</i>
      </p>
    </div>
  );
};

export default ErrorPage;
