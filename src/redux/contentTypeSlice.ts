import { createSlice, PayloadAction } from "@reduxjs/toolkit";

const initialState: string = "list";

export const contentTypeSlice = createSlice({
  name: "contentType",
  initialState,
  reducers: {
    setContentType: (state, action: PayloadAction<string>) => {
      if (["tile", "list"].some((el) => el === action.payload)) {
        state = action.payload;
      }
      return state;
    },
  },
});

export const { setContentType } = contentTypeSlice.actions;
export default contentTypeSlice.reducer;
