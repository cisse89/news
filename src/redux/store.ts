import { configureStore, combineReducers, type PreloadedState } from "@reduxjs/toolkit";
import contentTypeSlice from "./contentTypeSlice";
import numOfArticlesSlice from "./numOfArticlesSlice";

const rootReducer = combineReducers({
  contentType: contentTypeSlice,
  numOfArticles: numOfArticlesSlice,
});

export const setupStore = (preloadedState?: PreloadedState<RootState>) => {
  return configureStore({
    reducer: rootReducer,
    preloadedState,
  });
};

export const store = setupStore();

export type RootState = ReturnType<typeof rootReducer>;
export type AppStore = ReturnType<typeof setupStore>;
export type AppDispatch = AppStore["dispatch"];
