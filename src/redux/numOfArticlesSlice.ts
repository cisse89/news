import { createSlice, PayloadAction } from "@reduxjs/toolkit";

const initialState: number = 0;

export const numOfArticlesSlice = createSlice({
  name: "numOfArticles",
  initialState,
  reducers: {
    setNumOfArticles: (state, action: PayloadAction<number>) => {
      return action.payload;
    },
  },
});

export const { setNumOfArticles } = numOfArticlesSlice.actions;
export default numOfArticlesSlice.reducer;
