import Content, { loader as articlesLoader } from "./components/Content";
import Header from "./components/Header";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import Root from "./routes/Root";
import ErrorPage from "./routes/ErrorPage";
import Footer from "./components/Footer";
import { useContext, useState } from "react";
import Translation from "./types/Translation.types";
import { english } from "./languages/english";
import TranslationContext from "./languages/TranslationContext";
import { getTranslation } from "./languages/translationMap";
import DefaultContent from "./routes/DefaultContent";
import "./App.css";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "/",
        element: <DefaultContent />,
      },
      {
        path: "country/:id",
        element: <Content />,
        loader: articlesLoader,
      },
    ],
  },
]);

function App() {
  const [translation, setTranslation] = useState<Translation>(english);

  const onChangeLanguage = (code: string) => {
    const newTranslation = getTranslation(code);
    setTranslation(newTranslation);
  };
  return (
    <TranslationContext.Provider value={translation}>
      <div>
        <div className="page-header">
          <Header changeLanguageCallback={onChangeLanguage} />
        </div>
        <div className="page-content">
          <div className="ui container">
            <RouterProvider router={router} />
          </div>
        </div>
        <div className="page-footer">
          <Footer />
        </div>
      </div>
    </TranslationContext.Provider>
  );
}

export default App;
