# Overview

Sample app using news api to generate content on the page `https://newsapi.org/`

Displays articles from selected country. There are two possible layouts, list and tiles, can be changed with button in header.

Supposed to be tiles only but it looks like only United Stated has an image to display.

# Used features and libraries

- functional components
- hooks
- context
- modal
- Redux Toolkit
- React Router DOM
- axios
- jest for unit testing

# Launch

To start app run `npm install && npm start` from the command line

# Testing

There are tests available, to launch use `npm test`
